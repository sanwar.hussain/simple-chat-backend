import http from 'http'
import { Server } from 'socket.io'

const PORT = 5000

const server: http.Server = http.createServer((req, res) => {
	res.statusCode = 200
	res.setHeader('Access-Control-Allow-Origin', '*')
	res.setHeader('Access-Control-Allow-Methods', 'GET')
	res.write('Connected!')
	res.end()
})

const io: Server = new Server(server, {
	cors: {
		origin: [
			'http://simplechat.gq',
			'https://simplechat.gq',
			'http://localhost:3000',
		],
	},
})

io.on('connection', (socket) => {
	socket.on('send-message', (message, roomKey) => {
		socket.to(roomKey).emit('receive-message', message)
	})

	socket.on('notify-user', (user, roomKey) => {
		const key = roomKey || socket.id

		if (user.type === 'connect') socket.join(key)
		socket.to(key).emit('receive-message', user)
		if (user.type === 'disconnect') socket.disconnect()
	})

	socket.on('disconnect', (data) => {})
})

server.listen(process.env.PORT || PORT, () => {
	console.log('Server listening on port', PORT)
})
